# Tiny Tag Scanner


![App Logo](images/App_logo.png "Tiny Tag Scanner logo")

**Tiny Tag Scanner** is a tiny qr code and NFC tag scanner created as tutorial app for 
the ISMB [cordova seminar](https://bitbucket.org/MobileSolutions_ISMB/cordova-tutorial).

## Usage

Just scan a QR code using the "Scan QR" button or tap a NFC tag.  
Scanned items will be shown in a list. If the scanned item is a URL, it will be
highlighted so that you can open it using the system browser.

## Cross-platform

Using Apache Cordova, this simple app supports Android, Windows and iOS.  
NFC will be available only on some devices and/or platforms.  
QR code scanning requires a device with a camera.

### Supported platforms

![App Logo](images/screenshots.jpg "Screenshot of the app workin on all 3 supported platforms")

* Android >= 5.0
* Windows >= 8.1 (including Desktop and Device)
* iOS >= 9

## Build

```bash
npm install
npm run-script build-debug-app
cordova build [platform]
```

Now either open the project in `platform/<platform name>` within your target platform
IDE (Android Studio, Visual Studio, XCode) or use the run command:

`cordova run PLATFORM [--device|--emulator]`.

#### Windows 10

Since the barcode scanner uses a native library that gets compiled separately, it is 
not possible to use the default *any cpu* target. See [here](https://github.com/phonegap/phonegap-plugin-barcodescanner#supported-platforms).
Specify target ARM for most Windows Phones.