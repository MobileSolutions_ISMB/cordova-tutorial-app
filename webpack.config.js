'use strict';
//use strict directive is required in Node.js in order to use (almost all) ES6 features
//without using Babel. Babel is a great tool but using it here would be an overkill 

//Node internal modules
const path = require('path');

//Webpack
const webpack = require('webpack');

//Other Dependencies
const HtmlWebpackPlugin = require('html-webpack-plugin'); //helps generating the HTML files
const autoprefixer = require('autoprefixer'); //Adds prefixes such as -webkit or -moz where required to our CSS/SCSS

//If we are going to include markdown in our application
//Useful when there are a lot of texts
const marked = require("marked");
//This is the renderer for markdown loader. Its default behavior can be modified
let renderer = new marked.Renderer();

//We can use info in our package.json straightly into our app
const pkg = require('./package.json');

//Some configuration variables you can use based on the process.env environment variables
//To set an env variable just type it before webpack commang, e.g. MY_ENV_VAR webpack
const IS_DEV_SERVER = (/webpack-dev-server/i).test(process.argv[1]);
const PRODUCTION_MODE = process.env.NODE_ENV === 'production';
const ENVIRONMENT = PRODUCTION_MODE ? 'production' : 'debug';
//you may decide to change output folder depending on some variable, e.g.
//if you are compiling for Cordova, but also as standalone web app
const OUTPUT_PATH = 'www';
//Status bar color - also for mobile chrome and wp tiles in web app mode
//package.json config field: see https://docs.npmjs.com/files/package.json#config
const STATUSBAR_COLOR = pkg.config.themeColor;
//Equivalent of C++ main.cpp for those who are familiar
const ENTRIES = {
    //This is a single entry point app/single target build but we could have more
    //the output name is the key, while the source is the value
    main: 'main.js'
};

//Put some definitions as constants we can use in our code
let definePlugin = new webpack.DefinePlugin({
    TITLE: JSON.stringify(pkg.title),
    VERSION: JSON.stringify(pkg.version),
    PKG_NAME: JSON.stringify(pkg.name),
    DESCRIPTION: JSON.stringify(pkg.description),
    ENVIRONMENT: JSON.stringify(ENVIRONMENT),
    BUILD_DATE: JSON.stringify(new Date()),
    STATUSBAR_COLOR: STATUSBAR_COLOR
});
//Path where our base jade template is located
let htmlTemplatePath = path.join(__dirname, 'src', 'views', 'index.jade');
//Renders into index.html
let htmlPlugin = new HtmlWebpackPlugin({
    language: 'en',
    themeColor: pkg.config.themeColor,
    template: htmlTemplatePath,
    favicon: '../images/platforms/browser/favicon.ico', //more complex behavior can be added for web apps! - this is just for debug. Path relative to src 
    title: pkg.title,
    filename: 'index.html',
    isCordova: !IS_DEV_SERVER,
    chunks: ['main'] // we could have more scripts here - see https://github.com/ampedandwired/html-webpack-plugin#filtering-chunks
});

//Our base set of webpack plugins
let plugins = [htmlPlugin, definePlugin];

//Code optimization for production mode: strip comments and white spaces, etc...
if (PRODUCTION_MODE) {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
        sourceMap: false //set this to true if you want .map files
    }));
    plugins.push(new webpack.optimize.OccurenceOrderPlugin());
}


const webpackConfig = {
    //We are always referring to src or node_modules folders in our app - see modulesDirectories
    context: path.join(__dirname, 'src'),
    entry: ENTRIES,
    output: {
        //JS Output will be in OUTPUT_PATH/APP_ENTRY_POINT.js
        path: OUTPUT_PATH,
        filename: '[name].js'
    },
    module: {
        loaders: [{
            test: /\.js?$/,
            exclude: /(node_modules)/,
            loader: 'babel',
            query: {
                presets: ['es2015']
            }
        }, {
                test: /\.md$/,
                loaders: ['html', 'markdown']
            }, {
                test: /\.jade$/,
                loader: 'jade'
            }, {
                test: /\.json$/,
                loader: 'json'
            }, {
                test: /\.scss$/,
                loaders: ['style', 'css', 'postcss', 'sass']
            }, {
                test: /\.css$/,
                loaders: ['style', 'css', 'postcss']
            }, {
                test: /\.woff2?(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=application/font-woff"
            }, {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=application/octet-stream"
            }, {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file"
            }, {
                test: /\.ico(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file"
            }, {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=10000&mimetype=image/svg+xml"
            }, {
                test: /\.jpg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=250000&mimetype=image/jpeg"
            }, {
                test: /\.png(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url?limit=250000&mimetype=image/png"
            }]
    },
    resolve: {
        //We are always referring to src or node_modules folders in our app - see modulesDirectories
        extensions: ['', '.js', '.jade', '.md', '.json'],
        modulesDirectories: ['node_modules', 'src']
    },
    plugins: plugins,
    //This will apply css vendor prefixes where needed - no need to worry anymore about -webkit, -ms, -moz
    postcss: function () {
        return [autoprefixer];
    },
    //Set renderer for markdown - there are also other renderers with predefined styles - see marked
    markdownLoader: {
        renderer: renderer
    },
    devtool: 'source-map'
}

module.exports = webpackConfig;