import dialogPolyfill from 'dialog-polyfill'; //see https://getmdl.io/components/index.html#dialog-section
import moment from 'moment'; //a date formatting library

//The view
let infoPopupTemplate = require('views/infoPopup.jade');

class InfoPopup {
    constructor(dialogElement) {
        this.element = dialogElement;
        let cordova = typeof cordova === 'undefined' ? {} : cordova;
        let device = typeof device === 'undefined' ? {} : device;
        this.element.innerHTML = infoPopupTemplate({
            //remember those constant in webpack.config --> define plugin
            title: TITLE,
            description: DESCRIPTION,
            appVersion: VERSION,
            buildDate: moment(BUILD_DATE).format('LLL'),
            cordova: window.cordova,
            device: window.device
        });
        //for non-chrome browser and webviews which do not support <dialog>
        if (typeof this.element.showModal !== 'function') {
            require('dialog-polyfill/dialog-polyfill.css');
            dialogPolyfill.registerDialog(this.element);
        }
        //patch dialog behavior forcing redraw
        //to avoid overflow in long dialogs
        //(no need to add/remove backbutton listener here)
        let onOrientationChange = () => {
            if (this.element.open) {
                let onCloseTemp = () => {
                    this.element.removeEventListener('close', onCloseTemp);
                    setTimeout(() => {
                        this.element.showModal()
                    }, 100);
                };
                this.element.addEventListener('close', onCloseTemp);
                this.element.close();
            }
        };

        window.addEventListener('orientationchange', onOrientationChange, false)

        //This will bind scope to 'this'
        //we could have defined close like show and loose the scope binding
        this.close = () => {
            //remove backbutton listener
            document.removeEventListener('backbutton', this.close);
            this.element.close();
        }

        this.element.querySelector('button.close').addEventListener('click', this.close);
    }

    show() {
        //add cordova hardware backbutton listener (if present)
        //will prevent from closing app, closing just the popup
        document.addEventListener('backbutton', this.close);
        this.element.showModal();
    }
}

export default InfoPopup;

export {
InfoPopup
};