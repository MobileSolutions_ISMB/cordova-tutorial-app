//A javascript swiss army knife
import _ from 'lodash';

//A template for each list item
let listItemTemplate = require('views/listItem.jade');

const IS_URL = /^(http:\/\/|^https:\/\/)(\w)+\.(\w)+/i;

const DEFAULT_OPTIONS = {
    hasNFC: false
};

class Application {
    constructor(element, options) {
        this.element = element;
        this.options = _.defaults(options, DEFAULT_OPTIONS);

        //Bind clear list handlers
        let clearButton = this.element.querySelector('button.clear-all');
        clearButton.addEventListener('click', () => {
            this.clearList();
        });

        //Bind QR Code scan handlers
        let scanQRButton = this.element.querySelector('button.scan-qr');
        scanQRButton.addEventListener('click', () => {
            //Barcode plugin handlers
            let onScanSuccess = (data) => {
                if (!data.cancelled) {
                    let item = {
                        icon: 'linked_camera',
                        isUrl: IS_URL.test(data.text),
                        content: data.text
                    };
                    this.addItemToList(item);
                }
                //else user cancelled capture
            };
            let onScanError = function(e) {
                console.error("scan error", e);
            };
            cordova.plugins.barcodeScanner.scan(onScanSuccess, onScanError);
        });

        //Bind NFC read handlers
        if (this.options.hasNFC === true) {
            //avoid duplicates
            let lastNFCPayload = '';
            //add listener for NDEF NFC messages
            let ndefHandler = (event) => {
                let ndefMessage = event.tag.ndefMessage;
                let payloads = [];
                for (var i = 0; i < ndefMessage.length; i++) {
                    // we are reading only NFC NDEF Well known types
                    if (ndefMessage[i].tnf === ndef.TNF_WELL_KNOWN) {
                        let type = nfc.bytesToString(ndefMessage[i].type);
                        let payload = '';
                        switch (type) {
                            case 'T':
                                //if is text tag
                                payload = ndef.textHelper.decodePayload(ndefMessage[i].payload);
                                break;
                            case 'U':
                                //if is uri tag
                                payload = ndef.uriHelper.decodePayload(ndefMessage[i].payload);
                                break;
                            default:
                                //we ignore other types
                                break;
                        }
                        if (payload.length > 0) {
                            //Avoid duplicate readings
                            if (payload !== lastNFCPayload) {
                                lastNFCPayload = payload;
                                payloads.push(payload);

                                let item = {
                                    icon: 'nfc',
                                    isUrl: IS_URL.test(payload),
                                    content: payload
                                };
                                this.addItemToList(item);
                            }
                        }
                    }
                }
            };
            nfc.addNdefListener(ndefHandler);
        }
        //else nfc not available
    }

    //Update items list
    addItemToList(item) {
        let startMessage = this.element.querySelector('div.start-message');
        let ul = this.element.querySelector('ul.mdl-list');
        //instantiate node from template
        let temp = document.createElement('div');
        temp.innerHTML = listItemTemplate({
            item: item
        });
        ul.appendChild(temp.children[0]);
        if (startMessage.style.height === '') {
            startMessage.style.height = 0;
        }
    }

    //clear items list
    clearList() {
        let ul = this.element.querySelector('ul.mdl-list');
        ul.textContent = '';
        //reset message size
        this.element.querySelector('div.start-message').style.height = '';
    }
}

export default Application;

export {
    Application
};