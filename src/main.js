require('style/stylesheets.scss');
import 'material-design-lite/material'; //Import MD (componentHandler) in the global namespace

import Application from 'js/Application';
import InfoPopup from 'js/InfoPopup';

function startApp(containerElement, hasNFC) {
    let app = new Application(containerElement, { hasNFC: hasNFC });
}

function onStart() {
    console.log("App started / cordova ready");
    let containerElement = document.querySelector('.app-container');

    if (typeof window.nfc === 'undefined') {
        //NFC not available for current platform/environment (e.g. browser)
        containerElement.classList.add('qr-only');
        startApp(containerElement, false);
    }
    else {
        //Check if nfc is available on device
        nfc.enabled(() => {
            containerElement.classList.add('nfc-and-qr');
            startApp(containerElement, true);
        }, () => {
            //NFC absent or switched off
            containerElement.classList.add('qr-only');
            startApp(containerElement, false);
        });
    }
    //Init dialog
    let dialog = new InfoPopup(document.querySelector('dialog.info-popup'));
    document.querySelector('button.about-app').addEventListener('click', ()=>{
       dialog.show(); 
    });
}

//Debug only
if (typeof cordova === 'undefined') {
    document.addEventListener('DOMContentLoaded', onStart);
}
else {
    document.addEventListener('deviceready', onStart);
}
