#!/usr/bin/env node

//This hook copies android styles.xml to android platform.
//This can be used to add support for Lollipop+ features such as navbar or status bar colors
var fs = require('fs');
var path = require('path');

var rootdir = path.resolve(__dirname, '../../');

var androidProjectDir = path.join(rootdir, 'platforms', 'android', 'res', 'values');

var androidStyle = path.join(rootdir, 'app_assets', 'android', 'styles.xml');

function _onFileExists(exists) {
    if (exists) {
        console.log("Coping styles.xml into", androidProjectDir);
        fs.createReadStream(androidStyle).pipe(fs.createWriteStream(path.join(androidProjectDir, 'styles.xml')));
    } else {
        console.error("Android project directory not found");
    }
}

fs.exists(androidProjectDir, _onFileExists);
