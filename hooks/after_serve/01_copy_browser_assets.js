#!/usr/bin/env node
//Copy files needed from the browser platform
'use strict';
console.log("Copy files for serve");

const path = require('path');
const fs = require('fs-extra');

const rootdir = path.resolve(__dirname, '../../');
const browserAssets = path.join(rootdir, 'images', 'platforms', 'browser');
const configFile = path.join(rootdir, 'config.xml');
const platformsDir = path.join(rootdir, 'platforms');

console.log("rootdir", rootdir)

fs.copySync(browserAssets, path.join(platformsDir, 'browser', 'www'));
fs.copySync(configFile, path.join(platformsDir, 'browser', 'www', 'config.xml'));