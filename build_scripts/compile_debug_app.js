#!/usr/bin/env node

process.env.USE_CORDOVA = true;
process.env.NODE_ENV = 'debug';

var childProcess = require('child_process');
var webpackProcess = childProcess.exec('webpack');
webpackProcess.stdout.on('data', function(data) {
    console.log('Webpack Output\n: ' + data);
});

webpackProcess.stderr.on('data', function(data) {
    console.log('Webpack Error\n: ' + data);
});

webpackProcess.on('exit', function(code) {
    console.log('Webpack process exited with code ' + code);
    if (code === 0) {
        var cordovaPrepare = childProcess.exec('cordova prepare');
        cordovaPrepare.stdout.on('data', function(data) {
            console.log('--- Cordova prepare Output\n: ' + data);
        });

        cordovaPrepare.stderr.on('data', function(data) {
            console.log('--- Cordova Error\n: ' + data);
        });

        cordovaPrepare.on('exit', function(code) {
            console.log('Cordova prepare process exited with code ' + code);
        });
    }
});
